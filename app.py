import speech_recognition as sr
import pickle
import os
import malaya as m
import json
from monkeylearn import MonkeyLearn

from emotion_recog import extract_feature

def recog_speech():
    # prepare audio for recognition
    speech = sr.AudioFile(filename)
    with speech as source:
        audio = r.record(source)

    # recognize speech and return result 
    text = r.recognize_google(audio, language=lang)
    return text

def recog_emotion():
    # load the saved model (after training)
    model = pickle.load(open("model/emotionrecog.model", "rb"))

    # extract features and reshape it
    features = extract_feature(filename, mfcc=True, chroma=True, mel=True).reshape(1, -1)

    # predict emotion
    result = model.predict(features)

    # display result
    print ("Detected emotion: ", result)

def analyse_sentiment():
    # use different model for different language
    if lang == "ms-MY":
        # load sentiment model 'albert' from malaya and run on text
        multinomial = m.sentiment.multinomial()
        alxlnet = m.sentiment.transformer(model = 'alxlnet')
        xlnet = m.sentiment.transformer(model = 'xlnet')
        sentiment = multinomial.predict_proba([text])     
    elif lang == "en-US":
        # private MonkeyLearn API key, do not distribute
        ml = MonkeyLearn('ff1cfdaf494ec22525462f2749bdafd87a582890')
        data = [text]
        model_id = 'cl_pi3C7JiL'
        result = ml.classifiers.classify(model_id, data).body

        # retrieve sentiment label from dictionary
        sentiment = result[0]["classifications"][0]["tag_name"]
    
    print ("Detected sentiment: ", sentiment)

def detect_topic():
    # load transformer model 'alxlnet' from malaya
    model = m.zero_shot.classification.transformer(model = 'alxlnet')

    # run prediction on labels
    result = model.predict_proba([text], labels = ['putusan elektrik', 'bantuan', 'penama', 'aduan', 'bayar', 'semakan'])
    
    # sort labels by likeliness
    sorted_result = sorted(result[0].items(), key=lambda x: x[1], reverse=True)

    print ("Most to least likely topic: ", sorted_result)

if __name__ == "__main__":
    # initiate speech recognizer and set language (Malay: ms-MY, English: en-US)
    r = sr.Recognizer()
    lang = "ms-MY"

    # load audio file to be used
    filename = "demo/Demo_H1_my.wav"

    # run speech recognition and display result
    text = recog_speech()    
    
    # custom sentence
    text = "Saya telefon ni sebab nak tahu selain dari pergi, ada cara lain tak untuk saya bayar bil?"
    
    print("Recognized speech: ", text)

    # run emotion recognition
    recog_emotion()    

    # run sentiment analysis
    analyse_sentiment()

    # run topic classification
    detect_topic()
